'use strict'
// Change color of navigation panel with hover
const nav = document.querySelector('.header__nav');
const page = document.getElementById('page');
const addActivItem = function (e) {
    if (e.target.classList.contains('header__nav') == false) {
        for (let i = 0 ; i < nav.children.length; i++) {
            nav.children[i].classList.remove('active-item');
        }
        e.target.classList.add('active-item');    
    }
}
const removeActiveItem = function (e) {
    e.target.classList.remove('active-item');
    if (page.classList.contains('active-item') == false) {
        page.classList.add('active-item');
    }  
}
nav.addEventListener('mouseover', addActivItem);
nav.addEventListener('mouseout', removeActiveItem);

// Change style of price buttons Monthly or Yearly
const select = document.querySelector('.price__select');
const price = document.querySelector('.price__price');
const addActivePrice = function (e) {
    e.target.classList.add('active-price');
    if (e.target == select.children[1]) {
        select.children[0].classList.remove('active-price');
    } else {
        select.children[1].classList.remove('active-price');
    }
}
const removeActivePrice = function (e) {
    e.target.classList.remove('active-price');
    if (price.classList.contains('month')) {
        select.children[0].classList.add('active-price');
    } else {
        select.children[1].classList.add('active-price');
    }
}
select.addEventListener('mouseover', addActivePrice);
select.addEventListener('mouseout', removeActivePrice);

new Swiper ('.swiper-container', {
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },

    slidesPerView: 2,
    spaceBetween: 35,
});

const openBtn = document.getElementById('open');
const closeBtn = document.getElementById('close');
const menu = document.querySelector('.header__nav');
closeBtn.classList.add('none');
menu.classList.add('none');
console.log(openBtn);
console.log(closeBtn);
console.log(menu);

const open = function () {
    openBtn.classList.add('none');
    closeBtn.classList.remove('none');
    menu.classList.remove('none');
}

const close = function () {
    openBtn.classList.remove('none');
    closeBtn.classList.add('none');
    menu.classList.add('none');
}

openBtn.addEventListener('mousedown', open);
closeBtn.addEventListener('mousedown', close);
